# singi-kva-2019

Raspored tema po nedeljama nastave:

## 1) 26.2.2019.utorak
  - Angular 6. podesavanje okruzenja
  - instalacija Angular CLI
  - kreiranje jednostavnog projekta sa jednom jednostavnom komponentom
  - serviranje aplikacije
  - Student ce umeti da napravi jednostavan primer Angular 6 aplikacije

## 2) 5.3.2019. utorak
  - Typescript 
  - Zasto uopste TS?
  - Prevodjenje u JS? 
  - Dosta primera u kojima se demonstriraju svi elementi TS

## 3) 12.3.2019. utorak
   - Arhitektura Angular aplikacije
   - Komponente i sabloni
   - Razmena poruka publisher - subscriber
   - Bootstraping
   - NgModules
   - Dependency Injection

## 4) 19.3.2019. utorak
   - HttpClient
   - Routing & navigation
   - Animations
## 5) 26.3.2019. utorak
   - Slozeni primeri u kojem se demonstriraju funkcije:
      - forma, lista, pretrazivanje, tree-view
## 6) 2.4.2019. utorak
    - Kolokvijum
    - ocena 6: kreirati jednostavnu angular 6 aplikaciju sa jednom listom, dodavanjem elementata u listu, obrada i pretrazivanje
    - ocena 8: prosiriti prethodni primer sa slozenijom navigacijom
    - ocena 10: prosiriti prethodne primere sa koriscenjem asinhronih HTTP poziva

## 7) 9.4.2019. utorak
    - Koriscenje eksternih modula/javascript biblioteka 
    - D3.js

## 8) 16.4.2019. utorak
    - Koriscenje eksternih modula
    - Interaktivni kalendar i povezivanje sa google kalendarom

## 9) 23.4.2019. utorak
    - Koriscenje eksternih modula
    - FileUpload
    - WEBSOCKET
    - Chat

## 10) 30.4.2019. utorak
    - Koriscenje eksternih modula
    - openlayers/mapa

## 11) 7.5.2019. utorak
    - WebGL
    - Three.js

## 12) 14.5.2019. utorak
    - Kolokvijum II
    - ocena 6: kreirati angular aplikaciju sa prikazom nekoliko vrsta grafikona
    - ocena 8: interaktivno kreiranje rasporeda predavanja
    - ocena 10: prethodno sa mogucnoscu interakcije vise korisnika

## 13) 21.5.2019. utorak
    - Testiranje Protractor
    - Deployment: WebPACK
## 14) 28.5.2019. utorak
    - ReactJS
    - VueJS
## 15) 4.6.2019. utorak
    Zavrsni projekti/ priprema
