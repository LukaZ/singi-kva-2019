import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'; //  npm install d3

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'd3primer';

  ngOnInit() {
    this.drawChart();
  }


  drawChart():void{

    let margin = {top: 10, right: 30, bottom: 30, left: 60};
    let width = 460 - margin.left - margin.right;
    let height = 400 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    let svg = d3.select("#my_dataviz")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");


    // Add X axis
    let x = d3.scaleLinear()
      .domain([4, 8])
      .range([ 0, width ]);
    
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

    // Add Y axis
    var y = d3.scaleLinear()
      .domain([0, 9])
      .range([ height, 0]);
    
    svg.append("g").call(d3.axisLeft(y));

    // // Add dots
    let data = [
      {x:4, y:0.2, color:"#440154ff"},
      {x:4.2, y:8, color:"#21908dff"},
      {x:5.4, y:8, color:"#21908dff"},
      {x:6.6, y:6, color:"#21908dff"},
      {x:7.7, y:7, color:"#21908dff"},
      {x:7.0, y:8, color:"#21908dff"},
      {x:7.2, y:3, color:"#21908dff"}
    ]

    for(let i=4; i<8; i+=0.15){
      data.push({
        x:i,
        y:(4+4*Math.sin(i)),
        color: "#21908dff"
      })
    }

    svg.append('g')
     .selectAll("dot")
     .data(data)
     .enter()
     .append("circle")
       .attr("cx", function (d) { return x(d.x); } )
       .attr("cy", function (d) { return y(d.y); } )
       .attr("r", 5)
       .style("fill", function (d) { return d.color } )






  }


}
