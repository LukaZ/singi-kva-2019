"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var message = 'Hello world dva';
console.log(message);
var x = 10;
var y = 3;
x = 4;
var sum = 34;
sum += 3;
var sablon = "Suma brojeva " + sum + "\n=================================\n";
sablon = sablon.toLowerCase();
console.log(sum);
console.log(sablon);
// LISTA
var lista = [1, 2, 3, 4];
var lista2 = [1, 4, 5];
var persons = ['Pera', 3];
// enumeracija
var Color;
(function (Color) {
    Color[Color["Red"] = 5] = "Red";
    Color[Color["Blue"] = 6] = "Blue";
    Color[Color["Green"] = 7] = "Green";
})(Color || (Color = {}));
var boja = Color.Green;
console.log(boja);
var proba = [1, 3, 'proba'];
proba = 'nesto';
var b = '10';
b.toUpperCase();
var b2 = 20;
//b = 'tre';
var c1; // MULTI TIP
c1 = 'pera';
c1 = 3;
// c1 = true;
// funkcije -----
function add(num) {
    return num + 3;
}
var s = add(3);
console.log(s);
// opcioni parametri
function add2(num1, num2) {
    if (num2) {
        return num1 + num2;
    }
    else {
        return num1 + 3;
    }
}
function add3(num1, num2) {
    if (num2 === void 0) { num2 = 10; }
    return num1 + num2;
}
console.log(add3(4));
// interfejsi -------
function fullName(person) {
    console.log("" + person.firstName);
    return person.firstName;
}
fullName({ firstName: 'pera', lastName: 'petrovic' });
var nesto = { firstName: 'milan', lastName: 'marko' };
nesto.firstName = 'Milan';
function fullName2(person) {
    console.log("" + person.firstName);
}
fullName2(nesto);
// klase ----------
var Student = /** @class */ (function () {
    function Student(name) {
        this.firstName = name;
    }
    Student.prototype.hello = function () {
        console.log('Student ' + this.firstName);
    };
    return Student;
}());
var st1 = new Student('pera');
st1.hello();
var SiitStudent = /** @class */ (function (_super) {
    __extends(SiitStudent, _super);
    function SiitStudent(name) {
        return _super.call(this, name) || this;
    }
    SiitStudent.prototype.znaTypescript = function () {
        console.log("Znam da programiram typescript");
    };
    return SiitStudent;
}(Student));
var st2 = new SiitStudent('marko');
st2.znaTypescript();
// modifikatori pristupa
// public - default, private, protected
var IrStudent = /** @class */ (function (_super) {
    __extends(IrStudent, _super);
    function IrStudent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IrStudent.prototype.znaDEVOP = function () {
        console.log('DEVOP');
    };
    return IrStudent;
}(Student));
var st3 = new IrStudent('maja');
st3.znaDEVOP();
var a = 3;
console.log(a);
