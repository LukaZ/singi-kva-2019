export{}
let message = 'Hello world dva';

console.log(message);

let x=10;

const y = 3;

x = 4;

let sum:number = 34;

sum += 3;

let sablon:string = `Suma brojeva ${sum}
=================================
`

sablon = sablon.toLowerCase()

console.log(sum)
console.log(sablon)


// LISTA
let lista: number[] = [1, 2, 3, 4];
let lista2: Array<number> = [1, 4, 5];
let persons: [string, number] = ['Pera', 3];

// enumeracija

enum Color {Red=5, Blue, Green}

let boja:Color = Color.Green;
console.log(boja)

let proba: any = [1, 3, 'proba'];
proba = 'nesto';


let b:unknown = '10';

(b as string).toUpperCase();

let b2 = 20;
//b = 'tre';

let c1: string|number; // MULTI TIP

c1 = 'pera';
c1 = 3;
// c1 = true;

// funkcije -----
function add(num:number):number{
    return num+3;
}

let s = add(3);
console.log(s);

// opcioni parametri
function add2(num1:number, num2?:number):number{
    if(num2){
        return num1+num2;
    }else{
        return num1+3;
    }
}

function add3(num1:number, num2:number =10):number{
    return num1+num2;
}

console.log(add3(4));


// interfejsi -------
function fullName(person: {firstName:string, lastName:string}):string{
    console.log(`${person.firstName}`)
    return person.firstName;
}

fullName({firstName: 'pera', lastName:'petrovic'})

interface Person{
    firstName: string,
    lastName?: string
}

let nesto:Person = {firstName:'milan', lastName:'marko'};

nesto.firstName = 'Milan';

function fullName2(person: Person){
    console.log(`${person.firstName}`)
}

fullName2(nesto);

// klase ----------
class Student{
    firstName: string;


    constructor(name:string){
        this.firstName = name;
    }

    hello(){
        console.log('Student '+this.firstName);
    }
}

let st1 = new Student('pera');

st1.hello();

class SiitStudent extends Student{

    constructor(name:string){
        super(name);
    }

    znaTypescript(){
        console.log("Znam da programiram typescript");
    }
}


let st2 = new SiitStudent('marko');
st2.znaTypescript();

// modifikatori pristupa
// public - default, private, protected

class IrStudent extends Student{
    private godina:number;

    znaDEVOP(){
        console.log('DEVOP');
    }
}

let st3 = new IrStudent('maja');
st3.znaDEVOP();


let a = 3;

console.log(a);
